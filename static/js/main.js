$(document).ready(function () {
  //  svg4everybody({});
});

$('.nav-link-header').click(function(e) {
    e.preventDefault();
    $('.nav-link-header').removeClass('nav-link-header--active');
    $(this).addClass('nav-link-header--active');
});

$('.testimonials-carousel').slick({
  centerMode: true,
  slideToShow: 3,
  variableWidth: true,
  prevArrow: $('.testimonial-arrow-left'),
  nextArrow: $('.testimonial-arrow-right'),
});
